<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\Models\User;
use App\Services\VacationCalculator;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use function is_float, is_array, count;

/**
 * Class CalculatorTest
 *
 * @package Tests\Unit
 */
class CalculatorTest extends TestCase
{
    public const CORRECT_GIVEN_YEAR = 2011;

    public const INCORRECT_GIVEN_YEAR = 1900;

    /** @var User */
    private $user;

    /** @var VacationCalculator */
    private $calculator;

    /**
     * @return void
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->calculator = $this->app->make(VacationCalculator::class);

        $this->user = User::query()->first();
    }

    /**
     * @return void
     */
    public function testCalculatorInstance(): void
    {
        $this->assertTrue($this->calculator instanceof VacationCalculator);
    }

    /**
     * @return void
     */
    public function testCalculatorSetYear(): void
    {

        $this->assertTrue(
            $this->calculator->setYear(static::CORRECT_GIVEN_YEAR) instanceof VacationCalculator
        );
    }

    /**
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function testCalculatorGetUserVacationDays(): void
    {
        if ($this->user) {
            $this->assertTrue(
                is_float($this->calculator->getUserVacationDays($this->user))
            );
        };
    }

    /**
     * @return void
     */
    public function testCalculatorCalculationPositive(): void
    {
        $data = $this->calculator
            ->setYear(static::CORRECT_GIVEN_YEAR)
            ->calculate();
        $this->assertTrue(
            is_array($data) && count($data) == 3
        );
    }

    /**
     * @return void
     */
    public function testCalculatorCalculationNegative(): void
    {
        $data = $this->calculator
            ->setYear(static::INCORRECT_GIVEN_YEAR)
            ->calculate();
        $this->assertTrue(count($data) == 1);
    }

    /**
     * @return void
     */
    public function testCalculatorFileGenerating(): void
    {
        $fileName = $this->calculator
            ->setYear(static::CORRECT_GIVEN_YEAR)
            ->generateReportFile();

        $this->assertTrue(file_exists($fileName));
    }

    /**
     * @return void
     */
    public function testCalculatorConsoleCommand(): void
    {
        $this->artisan('ottivo:calculate-vacations-days')
            ->expectsQuestion('Please set given year', static::INCORRECT_GIVEN_YEAR)
            ->expectsOutput(sprintf(
                'Please set years from %s to %s',
                VacationCalculator::MINIMUM_GIVEN_YEAR,
                (Carbon::now())->format('Y')
            ));

        $this->artisan('ottivo:calculate-vacations-days')
            ->expectsQuestion('Please set given year', static::CORRECT_GIVEN_YEAR)
            ->assertExitCode(0);
    }
}
