<?php

declare(strict_types = 1);

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use const true;
use function ceil, floor;

/**
 * Class VacationCalculator
 */
class VacationCalculator
{
    public const MINIMUM_GIVEN_YEAR = 1950;

    public const MINIMUM_VACATION_DAYS = 26;

    public const ADDITIONAL_VACATION_DAY_AGE = 30;

    public const ADDITIONAL_VACATION_DAY_PERIOD = 5;

    public const MONTHS_IN_YEAR = 12;

    /** @var */
    protected $year;

    /**
     * @param int $year
     *
     * @return \App\Services\VacationCalculator
     */
    public function setYear(int $year): VacationCalculator
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return string
     */
    public function generateReportFile(): string
    {
        $data = $this->calculate();
        $fileName = storage_path(
            'app/public/vacation_days_report_'.Carbon::now()->format('d_m_Y_H_i_s').'.csv'
        );
        $fp = fopen($fileName, 'w');

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);

        return $fileName;
    }

    /**
     * @return array
     */
    public function calculate(): array
    {
        $data = [
            [
                Lang::get('main.file.name'),
                Lang::get('main.file.vacation_days'),
            ]
        ];

        (User::all())->each(function ($user) use (&$data) {
            $vacationDays = $this->getUserVacationDays($user);

            if ($vacationDays) {
                $data[] = [
                    $user->getAttribute('name'),
                    $vacationDays,
                ];
            }
        });

        return $data;
    }

    /**
     * @param \App\Models\User $user
     *
     * @return float
     *
     * @throws \InvalidArgumentException
     */
    public function getUserVacationDays(User $user): float
    {
        $specialContract = $user->getAttribute('working_special_contract');
        $workingStartYear = (int) $user->getAttribute('working_start_date')
            ->format('Y');
        $workingStartMonth = (int) $user->getAttribute('working_start_date')
            ->format('n');
        $workingStartDay = (int) $user->getAttribute('working_start_date')
            ->format('j');
        $userAge = (int) $user->getAttribute('date_of_birth')->age;

        switch (true) {
            case $workingStartYear > $this->year:
                return 0;
                break;
            case $user->getAttribute('working_special_contract') !== null:
                return $specialContract;
                break;
            case $workingStartYear === $this->year:
                $monthsWithoutVacation = $workingStartDay === 1
                    ? $workingStartMonth
                    : $workingStartMonth + 0.5;
                $monthsWithVacation = static::MONTHS_IN_YEAR - $monthsWithoutVacation;

                $days = (static::MINIMUM_VACATION_DAYS / static::MONTHS_IN_YEAR) * $monthsWithVacation;

                return ceil($days);
                break;
            case $userAge >= static::ADDITIONAL_VACATION_DAY_AGE:
                $yearsAtWork = (Carbon::createFromDate($this->year, 1, 1))
                    ->diffInYears($user->getAttribute('working_start_date'));

                $additionalDays = floor($yearsAtWork / static::ADDITIONAL_VACATION_DAY_PERIOD);

                return static::MINIMUM_VACATION_DAYS + $additionalDays;
                break;
            default:
                return static::MINIMUM_VACATION_DAYS;
        }
    }
}
