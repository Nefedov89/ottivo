<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use App\Services\VacationCalculator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

/**
 * Class CalculateRequest
 *
 * @package App\Http\Requests
 */
class CalculateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => [
                'required',
                'numeric',
                'min:'.VacationCalculator::MINIMUM_GIVEN_YEAR,
                'max:'.(Carbon::now())->format('Y'),
            ],
        ];
    }
}
