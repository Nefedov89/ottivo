<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\CalculateRequest;
use App\Models\User;
use App\Services\VacationCalculator;
use Carbon\Carbon;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class MainController
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function home(): ViewContract
    {
        return View::make(
            'main',
            [
                'users' => User::all(),
            ]
        );
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fillDb(): RedirectResponse
    {
        Artisan::call('db:seed');

        Session::flash('success', Lang::get('main.messages.fill_db'));

        return Redirect::route('home');
    }

    /**
     * @param \App\Http\Requests\CalculateRequest $request
     * @param \App\Services\VacationCalculator $calculator
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function calculate(
        CalculateRequest $request,
        VacationCalculator $calculator
    ): BinaryFileResponse {
        $fileName = $calculator
            ->setYear((int) $request->get('year'))
            ->generateReportFile();

        return response()->download($fileName);
    }
}
