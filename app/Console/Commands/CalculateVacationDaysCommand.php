<?php

declare(strict_types = 1);

namespace App\Console\Commands;

use App\Services\VacationCalculator;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use function file_get_contents, sprintf;

class CalculateVacationDaysCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ottivo:calculate-vacations-days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate vacation days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Services\VacationCalculator $calculator
     *
     * @return mixed
     */
    public function handle(VacationCalculator $calculator)
    {
        $givenYear = (int) $this->ask('Please set given year');
        $presentYear = (Carbon::now())->format('Y');

        if ($givenYear < VacationCalculator::MINIMUM_GIVEN_YEAR || $givenYear > $presentYear) {
            $this->error(
                sprintf(
                    'Please set years from %s to %s',
                    VacationCalculator::MINIMUM_GIVEN_YEAR,
                    $presentYear
                    )
            );
        } else {
            $fileName = $calculator
                ->setYear((int) $givenYear)
                ->generateReportFile();

            $this->info(file_get_contents($fileName));
        }
    }
}
