<?php

declare(strict_types = 1);

return [
    'title'          => 'Welcome to Ottivo vacation days calculator',
    'empty_db'       => 'Oops. Looks like we don\'t have data in our database. Please push the button below',
    'fill_db_btn'    => 'Fill database with users',
    'messages'       => [
        'fill_db'         => 'Cool! Uses have been successfully added to database',
        'calculate_error' => 'Oops! Something went wrong',
    ],
    'table'          => [
        'name'                     => 'Name',
        'date_of_birth'            => 'Date of birth ',
        'working_start_date'       => 'Start date ',
        'working_special_contract' => 'Special contract',
        'vacation_days'            => 'vacation days',
    ],
    'form'           => [
        'year' => 'Please set given year',
        'btn'  => 'Calculate vacation days',
    ],
    'code_on_gitlab' => 'Code on gitlab',
    'file'           => [
        'name'          => 'Name',
        'vacation_days' => 'Vacation days',
    ],
    'console_tip' => 'Also you can run console command <span class="badge badge-info">php artisan ottivo:calculate-vacations-days</span> from the root of this project. See local set up instructions on gitlab'
];
