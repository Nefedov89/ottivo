<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ottivo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600"
          rel="stylesheet">

    <!-- Styles -->
    <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous"
    >
</head>
<body>
<div class="container py-5 text-center">
    <h1 class="text-center">@lang('main.title')</h1>
    <div class="row py-5">
        <div class="col-md-8 offset-md-2">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if(Session::has('errors'))
                <div class="alert alert-danger" role="alert">
                    @foreach(Session::get('errors') as $error)
                        <div>{{ Arr::first($error) }}</div>
                    @endforeach
                </div>
            @endif
            @if($users->count())
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('main.table.name')</th>
                        <th scope="col">@lang('main.table.date_of_birth')</th>
                        <th scope="col">@lang('main.table.working_start_date')</th>
                        <th scope="col">@lang('main.table.working_special_contract')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $i => $user)
                        <tr>
                            <th>{{ $i + 1 }}</th>
                            <td>{{ $user->getAttribute('name') }}</td>
                            <td>
                                {{ $user->getAttribute('date_of_birth')->format('d.m.Y') }}
                            </td>
                            <td>
                                {{ $user->getAttribute('working_start_date')->format('d.m.Y') }}
                            </td>
                            <td>
                                @if($user->getAttribute('working_special_contract'))
                                    {{ $user->getAttribute('working_special_contract') }}
                                    @lang('main.table.vacation_days')
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <form action="{{ route('calculate') }}" method="post">
                    @csrf
                    <div class="form-row align-items-center">
                        <div class="col-sm-5 my-1">
                            <input
                                name="year"
                                type="number"
                                min="{{ \App\Services\VacationCalculator::MINIMUM_GIVEN_YEAR }}"
                                max="{{ \Carbon\Carbon::now()->format('Y') }}"
                                step="1"
                                class="form-control"
                                placeholder="@lang('main.form.year')"
                            >
                        </div>
                        <div class="col-auto my-1">
                            <button type="submit" class="btn btn-primary">
                                @lang('main.form.btn')
                            </button>
                        </div>
                    </div>
                </form>
            @else
                <h4>@lang('main.empty_db')</h4>

                <form action="{{ route('fill-db') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-primary btn-lg">
                        @lang('main.fill_db_btn')
                    </button>
                </form>
            @endif
            <div class="mt-5">
                @lang('main.console_tip')
            </div>
            <div class="mt-5">
                <a href="https://gitlab.com/Nefedov89/ottivo" target="_blank">
                    @lang('main.code_on_gitlab')
                </a>
            </div>
        </div>

    </div>
</div>

<script
    src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"
></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"
></script>
<script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"
></script>
</body>
</html>
