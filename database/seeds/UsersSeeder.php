<?php

declare(strict_types = 1);

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

/**
 * Class UsersSeeder
 */
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function run()
    {
        $password = bcrypt('password');
        $usersData = [
            [
                'name'               => 'Hans Müller',
                'email'              => 'hm@test.com',
                'password'           => $password,
                'date_of_birth'      => Carbon::createFromFormat('d.m.Y', '30.12.1950'),
                'working_start_date' => Carbon::createFromFormat('d.m.Y','01.01.2001'),
            ],
            [
                'name'               => 'Angelika Fringe',
                'email'              => 'af@test.com',
                'password'           => $password,
                'date_of_birth'      => Carbon::createFromFormat('d.m.Y','09.06.1966'),
                'working_start_date' => Carbon::createFromFormat('d.m.Y','15.01.2001'),
            ],
            [
                'name'                     => 'Peter Klever',
                'email'                    => 'pk@test.com',
                'password'                 => $password,
                'date_of_birth'            => Carbon::createFromFormat('d.m.Y','12.07.1991'),
                'working_start_date'       => Carbon::createFromFormat('d.m.Y','15.05.2016'),
                'working_special_contract' => 27,
            ],
            [
                'name'                     => 'Marina Helter',
                'email'                    => 'mh@test.com',
                'password'                 => $password,
                'date_of_birth'            => Carbon::createFromFormat('d.m.Y','26.01.1970'),
                'working_start_date'       => Carbon::createFromFormat('d.m.Y','15.01.2018'),
            ],
            [
                'name'                     => 'Sepp Meier',
                'email'                    => 'sm@test.com',
                'password'                 => $password,
                'date_of_birth'            => Carbon::createFromFormat('d.m.Y','23.05.1980'),
                'working_start_date'       => Carbon::createFromFormat('d.m.Y','01.12.2017'),
            ],
        ];

        foreach ($usersData as $data) {
            $user = User::create($data);

            echo $user->getAttribute('name').' was created'.PHP_EOL;
        }
    }
}
