## Set up project locally

1. run in console `composer install`
2. run in console `cp .env.example .env`
3. in .env file set your APP_URL (http://example.dev), DB_DATABASE, DB_USERNAME, DB_PASSWORD  
4. run in console `php artisan key:generate`
5. run in console `php artisan migrate`
6. to run unit tests run in console `./vendor/bin/phpunit`


